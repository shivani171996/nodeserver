const http = require('http');
const fs = require('fs');
const path =  require('path');
const port = "3000";
const hostname = "localhost";

const server = http.createServer((req,res)=>{
    console.log(req.url,req.method);
    if(req.method=='GET'){
        var filePath,fileUrl;
        if(req.url=='/') fileUrl='/index.html';
        else fileUrl=req.url;
        filePath= path.resolve('./public'+fileUrl);
        console.log(filePath);
        const ext= path.extname(filePath);
        if(ext == '.html'){
            fs.exists(filePath,(exists)=>{
                if(!exists){
                    console.log("here");
                    res.statusCode= 404;
                    res.setHeader('Content-Type','text/html');
                    res.end('<html><body><h1>Error 404: ' + fileUrl + ' not found</h1></body></html>');
                    return;
                }
                res.statusCode= 200;
                res.setHeader('Content-Type','text/html');
                fs.createReadStream(filePath).pipe(res);
            });
        }
        else{
            res.statusCode= 404;
                    res.setHeader('Content-Type','text/html');
                    res.end('<html><body><h1>Error 404: ' + fileUrl + ' not found</h1></body></html>');

        }
    }
    else{
        res.statusCode= 404;
                    res.setHeader('Content-Type','text/html');
                    res.end('<html><body><h1>Error 404: ' + fileUrl + ' not found</h1></body></html>');

    }
});

server.listen(port,hostname,()=>
    console.log(`Server running at http://${hostname}:${port}/`));